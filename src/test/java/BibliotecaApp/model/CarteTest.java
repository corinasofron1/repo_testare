package BibliotecaApp.model;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class CarteTest {

    private Carte c1,c2,c3, c101;

    @BeforeClass
    public static void setUp() throws Exception{
        System.out.println("mesaj de inceput");
    }

    @Before
    public void declare_books(){
        c1 = new Carte();
        c2 = new Carte();
        c3 = new Carte();
        c1.setTitlu("Basme");
        //c2.setAutori(Arrays.asList("Ispirescu"));
        c1.setAnAparitie("1990");
        c2.setAnAparitie("1998");
        c3.setAnAparitie("1998");

        String an_aparitie1="1998";
        String an_aparitie2="1999";

        c101 = new Carte();
        c101.setAnAparitie("999");
        c101.setEditura("teora");

    }
// ********************************************************
    @Test
    public void assertFalseAutor() throws Exception{
        assertFalse("autor valid",
                (c101.getAutori().equals(null)));
        System.out.println("autorul nu poate fi null");
    }

    @Test
    public void assertFalseTitlu() throws Exception{
        assertFalse("titlu valid",
                (c101.getTitlu().equals(null)));
        System.out.println("titlul nu poate fi null");
    }

    @Test
    public void assertFalseLungimeTitlu() throws Exception{
        assertFalse("titlu valid",
                (c101.getTitlu().length()<=100) && (c101.getTitlu().length()>0));
        System.out.println("titlul trebuie sa fie intre 1 si 100 caractere");
    }

    @Test
    public void testAdd() throws Exception{
        Carte carte = new Carte();
        carte.setTitlu("povesti");
        carte.setAnAparitie("2000");



    }

    //***********************************************************************

    @Test
    public void getTitlu() throws Exception {
        assertEquals("Basme",c1.getTitlu());
        assertNotEquals("Poezii",c1.getTitlu());
        System.out.println("merge pt get titlu");
    }

    @Test
    public void setTitlu() throws Exception {
    }

    //@Test
    public void getAutori() throws Exception {
        assertEquals("Ispirescu",c2.getAutori());
        assertNotEquals("Eminescu",c2.getAutori());
    }


    @Test
    public void getAnAparitie() throws Exception {
        assertEquals("1998",c3.getAnAparitie());
        assertNotEquals("2000",c3.getAnAparitie());
        System.out.println("merge pt get an aparitie");
    }


    @Test
    public void setAnAparitie() throws Exception {
        Carte carte = new Carte();
        carte.setAnAparitie("2018");
        assertEquals("2018",carte.getAnAparitie());
    }


    //@Test
    public void cautaDupaAutor() throws Exception {
        assertEquals("Ispirescu",c2.getAutori());
    }


    @AfterClass
    public static void tearDown() throws Exception{
        System.out.println("mesaj de final");
    }

    //@Test (timeout = 10)
    public void test_ciclu_infinit() throws Exception{
        int i=1;
        while (i>0){
            i++;
            System.out.println(i);
        }
    }

    @Test
    public void assertTrueWithMessage() throws Exception{
        assertTrue("anul aparitiei cartii 3 este egal cu anul aparitiei cartii 2",
                c3.getAnAparitie()==c2.getAnAparitie());
        System.out.println("anul aparitiei cartii 3 este egal cu anul aparitiei cartii 2");
    }

    @Test
    public void assertFalseWithMessage() throws Exception{
        assertFalse("anul aparitiei cartii 3 este diferit de anul aparitiei cartii 2",
                c3.getAnAparitie()!= c2.getAnAparitie());
        System.out.println("anul aparitiei cartii 3 NU este diferit de anul aparitiei cartii 2");
    }

}