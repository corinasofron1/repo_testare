package BibliotecaApp.model.repo;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CartiRepoTest {

    CartiRepo cartiRepo;

    Carte ct1, ct2, ct3, ct4, ct5,
            ct6, ct7, ct8;


    @Before
    public void setUp()  {
        cartiRepo= new CartiRepo();

        // for test case 1
        ct1 = new Carte();
        ct1.setTitlu("basme 1");
        ct1.adaugaAutor("petre ispirescu");
        ct1.setAnAparitie("1499");
        ct1.setEditura("teora");
        ct1.adaugaCuvantCheie("ispirescu");

        // for test case 2
        ct2 = new Carte();
        ct2.setTitlu("basme 2");
        ct2.adaugaAutor("petre ispirescu");
        ct2.setAnAparitie("1500");
        ct2.setEditura("teora");
        ct2.adaugaCuvantCheie("aaa");

        // for test case 3
        ct3 = new Carte();
        ct3.setTitlu("basme 3");
        ct3.adaugaAutor("petre ispirescu");
        ct3.setAnAparitie("1501");
        ct3.setEditura("teora");
        ct3.adaugaCuvantCheie("ddddd");

        // for test case 4
        ct4 = new Carte();
        ct4.setTitlu("basme 4");
        ct4.adaugaAutor("petre ispirescu");
        ct4.setAnAparitie("2017");
        ct4.setEditura("teora");
        ct4.adaugaCuvantCheie("ddd");

        // for test case 5
        ct5 = new Carte();
        ct5.setTitlu("povestiri");
        ct5.adaugaAutor("petre ispirescu");
        ct5.setAnAparitie("2018");
        ct5.setEditura("teora");
        ct5.adaugaCuvantCheie("xxxxx");

        // for test case 6
        ct6 = new Carte();
        ct6.setTitlu("basme 6");
        ct6.adaugaAutor("petre ispirescu");
        ct6.setAnAparitie("2019");
        ct6.setEditura("teora");
        ct6.adaugaCuvantCheie("sss");

        // for test case 7
        ct7 = new Carte();
        ct7.setTitlu("M");
        ct7.adaugaAutor("eminescu");
        ct7.setAnAparitie("2021");
        ct7.setEditura("teora");
        ct7.adaugaCuvantCheie("eeee");

        // for test case 8
        ct8 = new Carte();
        ct8.setTitlu("album");
        ct8.adaugaAutor("barbu");
        ct8.setAnAparitie("2000");
        ct8.setEditura("teora");
        ct8.adaugaCuvantCheie("aaa");

    }


    @Test (expected = Exception.class) // se asteapta exceptie
    public void adaugaCarte1()  {
        boolean thrown = false;

        try {
            cartiRepo.adaugaCarte(ct1);
            System.out.println("The book has been added to the repository");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
    }

    @Test // se asteapta null
    public void adaugaCarte2() {

        int firstSize= cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(ct2);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("numar de carti inainte: "+firstSize);
        System.out.println("numar de carti dupa: "+lastSize);

        assertNotEquals(firstSize,lastSize);
    }

    @Test //se asteapta null
    public void adaugaCarte3() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(ct3);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("numar de carti inainte: "+firstSize);
        System.out.println("numar de carti dupa: "+lastSize);

        assertNotEquals(firstSize,lastSize);
    }

    @Test //se asteapta null
    public void adaugaCarte4() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(ct4);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("numar de carti inainte: "+firstSize);
        System.out.println("numar de carti dupa: "+lastSize);

        assertNotEquals(firstSize,lastSize);
    }

    @Test //se asteapta null
    public void adaugaCarte5() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(ct5);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("numar de carti inainte: "+firstSize);
        System.out.println("numar de carti dupa: "+lastSize);

        assertNotEquals(firstSize,lastSize);
    }


    @Test (expected = Exception.class)// se asteapta exceptie
    public void adaugaCarte6()  {
        boolean thrown = false;

        try {
            cartiRepo.adaugaCarte(ct6);
            System.out.println("The book has been added to the repository");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
    }


    @Test (expected = Exception.class)// se asteapta exceptie
    public void adaugaCarte7()  {
        boolean thrown = false;

        try {
            cartiRepo.adaugaCarte(ct7);
            System.out.println("The book has been added to the repository");
        } catch (Exception e) {
            thrown = true;
        }

        assertTrue(thrown);
    }

    @Test //se asteapta null
    public void adaugaCarte8() {

        int firstSize = cartiRepo.getCarti().size();
        cartiRepo.adaugaCarte(ct8);
        int lastSize = cartiRepo.getCarti().size();

        System.out.println("numar de carti inainte: "+firstSize);
        System.out.println("numar de carti dupa: "+lastSize);

        assertNotEquals(firstSize,lastSize);
    }


    @After
    public void tearDown() {
        ct1 =null;
        ct2 =null;
        ct3 =null;
        ct4 =null;
        ct5 =null;
        ct6 =null;
        ct7 =null;
        ct8 =null;
    }


    @Test
    public void getCarti() {
    }

    @Test
    public void modificaCarte() {
    }

    @Test
    public void stergeCarte() {
    }

    @Test
    public void cautaCarte() {
    }

    @Test
    public void getCartiOrdonateDinAnul() {
    }
}